<?php
/**
 * Created by PhpStorm.
 * User: Ye
 * Date: 26/07/2017
 * Time: 10:21
 */

/**
 * @param $data
 * @return string
 * comprobamos entrada de datos de un form
 * trim -> quita espacios
 * stripslashes -> quita barras \/
 * htmlspecialchars -> quita caracteres especiales
 */
function test($data){

    $data=trim($data);
    $data=stripslashes($data);
    $data=htmlspecialchars($data);
    return($data);
}