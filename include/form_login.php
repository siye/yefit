<!DOCTYPE html>
<html>
<?php
  //header
   include $_SERVER['DOCUMENT_ROOT'].'/include/header.php';
   ?>
<body>
<div class="parallax-container">
  <!--  formulario login-->  
    <div id="formulario"  class="row col s12 m4 l8 center formulario_login"  >
    <form method="POST" action="return false" onsubmit="return false" class="col s6 m6 l12 ">
        <div id="resultado" ></div>
        <div class="input-field inline ">
              <input type="text" name="user" id="user" value="" class="validate" required>
              
              <label for="user">Email</label>
        </div>
        <div class="input-field inline ">
             
              <input type="password" name="pass" id="pass" value="" class="validate" required>
              <label for="password">Password</label>
        </div> 
       
       <div class="input-field  ">
        <button onclick="Validar(document.getElementById('user').value, document.getElementById('pass').value);" class="btn waves-effect wves-light">Entrar</button>
      </div>
    </form>
  </div>

  <script>

  function Validar(user, pass)
  {
      $.ajax({
          url: "../modelo/validar.php",
          type: "POST",
          data: "user="+user+"&pass="+pass,
          success: function(resp){
          $('#resultado').html(resp)
          }       
      });
  }
  </script>
</div> <!-- fin container-->


    <!-- footer-->
     <?php  include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php'; ?>
    </body>
</html>