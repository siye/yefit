<footer class="page-footer">
          <div class="container">
            <div class="row">
              <div class="col l6 s12">
                <h5 class="white-text">Yefit</h5>
                <p class="grey-text text-lighten-4">Gestor de Gimnasio online, con sistema de recuperación de contraseñas via WhatsApp.</p>
                 <p class="grey-text text-lighten-4">Diseño responsive</p>
                 <p class="grey-text text-lighten-4">PHP, HTML5, CSS3, MaterializeCSS, SASS, JS, AJAX, MySql, GIT</p>
              </div>
              <div class="col l4 offset-l2 s12">
                <h5 class="white-text">Links</h5>
                <ul>
                  <li><a target="_blank" class="grey-text text-lighten-3" href="http://yemafe.es/portfolio/">portfolio YEMAFE</a></li>
                  
                </ul>
              </div>
            </div>
          </div>
          <div class="footer-copyright">
            <div class="container">
            © 2018 Copyright Proyecto DAW 2018 
            
            <!--<a class="grey-text text-lighten-4 right" href="#!">More Links</a>-->
            </div>
          </div>
        </footer>
      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
      <!--<script type="text/javascript" src="js/materialize.min.js"></script>-->
      <script type="text/javascript" src="js/bin/materialize.min.js"></script>
      
      <script>
        $(document).ready(function(){
            //lanzador parallax inicio
            $('.parallax').parallax();
            //lanzador menu desplegable actividades
            $(".dropdown-button").dropdown();
            

            $('.carousel.carousel-slider').carousel({fullWidth: true});

            //lanzador del menu comprimido xa movil
            $('.button-collapse').sideNav({
                  menuWidth: 200, // Default is 300
                });
            
           
 $('select').formSelect();

        });
        </script>

         
    </body>
  