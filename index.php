<!DOCTYPE html>
  <html>

<?php 
     include 'include/header.php';
    ?>
   
     

    <body>
     
     <!--parallax-->

      <div class="parallax-container">
        <div class="parallax"><img src="img/conLogo/ban_inicial_3_logo.jpg" width="100px"></div>
      </div>
      <div class="section white">
        <div class="row container">
          <h2 class="header">Yefit Tu centro deportivo</h2>
          <p class="grey-text text-darken-3 lighten-3">En yefit encontrarás las mejores actividades, impartidas por los mejores profesionales para conseguir tu cuerpo y forma física deseada.
          Ademas estarás en el centro más puntero con la ultima tecnología para controlar medir y gestionar mejor tu tiempo.</p>
        </div>
      </div>
      
      
    <div class="container"> 
     <!-- banners con actividades-->
     <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_pesas_logo.jpg">
              <span class="card-title">
               <a class=" waves-effect waves-light btn " href="bodypump.php"><b>BodyPump</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col col s12 m l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_spinning_logo.jpg">
              <span class="card-title"><b></b>
               <a class="waves-effect waves-light btn " href="cicloindoor.php"><b>Ciclo Indoor</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>
      
      <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_yoga_logo.jpg">
              <span class="card-title">
               <a class="waves-effect waves-light btn" href="yoga.php"><b>Yoga</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>
      

       <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_hit_logo.jpg">
              <span class="card-title ">
               <a class=" waves-effect waves-light btn " href="hit.php"><b>HIT</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_flexion_logo.jpg">
              <span class="card-title ">
               <a class=" waves-effect waves-light btn " href="cxwork.php"><b>CXWORK</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>

       <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_crossfit_logo.jpg">
              <span class="card-title ">
               <a class=" waves-effect waves-light btn " href="crossfit.php"><b>Crossfit</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>

      <div class="row">
        <div class="col s12 m12 l12">
          <div class="card card-index">
            <div class="card-image">
              <img src="img/conLogo/ban_abs_logo.jpg">
              <span class="card-title ">
               <a class=" waves-effect waves-light btn " href="abd.php"><b>Abdominales</b></a>
               </span>
            </div>
          </div>
        </div>
      </div>

      </div>
      
     <!-- footer-->
     
     <?php 
     include 'include/footer.php';
     
     ?>

     
     
    </body>
</html>