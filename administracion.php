
<!DOCTYPE html>
<html>

	<!-- header -->
	<?php include $_SERVER['DOCUMENT_ROOT'].'/include/header.php'; ?>
    <head>
    	<style type="text/css" media="screen">
    		#menu_socios,#menu_entrenadores,#menu_Actividades{
    			display: none;
    		}
    	</style>
    	<script  type="text/javascript" charset="utf-8" async defer>
    		/*js xa hacer visible menu de administracion de socios, empleados y actividades*/
    		
    		function ver_menu_socio(){
    			var entrenadores=document.getElementById("menu_entrenadores");
    			var socio=document.getElementById("menu_socios");
    			var actividades=document.getElementById("menu_Actividades");
    			if (socio.style.display = "none"){
    				socio.style.display="block";

    				entrenadores.style.display="none";
    				actividades.style.display="none";
    			}
    		}
    		function ver_menu_entrenadores(){
    			var entrenadores=document.getElementById("menu_entrenadores");
    			var socio=document.getElementById("menu_socios");
    			var actividades=document.getElementById("menu_Actividades");
    			if (entrenadores.style.display = "none"){
    				entrenadores.style.display="block";

    				socio.style.display="none";
    				actividades.style.display="none";
    			}
    		}
    		function ver_menu_actividades(){
    			var entrenadores=document.getElementById("menu_entrenadores");
    			var socio=document.getElementById("menu_socios");
    			var actividades=document.getElementById("menu_Actividades");
    			if (actividades.style.display = "none"){
    				actividades.style.display="block";

    				entrenadores.style.display="none";
    				socio.style.display="none";
    			}
    		}
    	</script>

</script>
    </head>
    <!-- /header -->
    <body>
	<div class="container">
<!-- boton de cerrar sesion-->
<a href="/include/logout.php" class="waves-effect waves-light btn right"><i class="material-icons  icono-nav ">directions_run</i>Cerrar Mi YeFit</a>

		<!-- botones administracion -->
		 
		    <div class="card-action ">
		    	<h2 class="texto_blanco">Administración de Yefit</h2>
		          <a  class="waves-effect waves-light btn " onclick="ver_menu_socio()"><i class="material-icons left icono-nav " >wc</i>Socios</a>
		          <a  class="waves-effect waves-light btn " onclick="ver_menu_entrenadores()"><i class="material-icons left icono-nav ">directions_bike</i>Entrenadores</a>
		          <a  class="waves-effect waves-light btn "onclick="ver_menu_actividades()"><i class="material-icons left icono-nav ">fitness_center</i>Actividades</a>
		    </div>

            <span id="resultado"></span><!-- para mostrar resultado del AJAX -->
		 
		<!-- listas administracion -->
		<!-- SOCIOS -->
		<div class="contenido_socio texto_blanco " id="menu_socios">
		<input id="btn_usuarios" type="button" class="btn btn-primary more" onclick="nuevoUsuario()" value="Nuevo Usuario"/><br/><br/>
		<div class="row col s12 m12 l12">
                <div >
                    <h3 class="panel-title">Lista de Socios</h3>
                </div>
                <div >
                    <table class="highlight centered  ">
                        <thead>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido1</th>
                        <th>Apellido2</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>WhatsApp</th>
                        <th>Editar</th>
                        <th>Eliminar</th>           
                        </thead>
                        <tbody>
                            <?php
                            require 'modelo/Database.php';
                            $pdo = Database::connect();
                            $sql = 'SELECT * FROM usuarios';
                            $con = 1;
                            
                            foreach ($pdo->query($sql) as $row) {
                                echo "<tr>";
                                echo '<td>' . $con . '</td>';
                                echo '<td>' . $row['nombre'] . '</td>';
                                echo '<td>' . $row['ape1'] . '</td>';
                                echo '<td>' . $row['ape2'] . '</td>';
                                echo '<td>' . $row['telefono'] . '</td>';
                                echo '<td>' . $row['email'] . '</td>';

                                //variables xa whatssap
                            $tel=$row['telefono'];
                            $usu=$row['email'];  
                            $sqlDos = 'SELECT * FROM login where email= "'.$row['email'].'";';
                            $password="";
                            foreach ($pdo->query($sqlDos) as $contra) {
                                
                                $password=$contra['password'];
                            }
                            //desenriptamos la contra
                            $contra=base64_decode($password);
       
                                $mensaje =addslashes("YeFit,%20Su%20usuario%20es:%20".$usu."%20y%20su%20Password:%20".$contra."");
                                echo '<td>
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=34'.$tel.'&text='.$mensaje.'">Recuperar Clave </a>
                                </td>';
 
                                echo '<td><button class="btn btn-primary" onclick="editaUsuario(' . $row['id'] . ')"><i class="material-icons">mode_edit</i></button></td>';
                                echo '<td><button class="btn btn-primary" onclick="eliminaUsuario(' . $row['id'] . ', this)"><i class="material-icons">delete</i></button></td>';
                                echo '</tr>';

                                $con++;

                            }
                            Database::disconnect();
                            ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
	</div><!-- fin contenido_socios-->	

	<!-- ENTRENADORES -->
		<div class="contenido_entrenadores texto_blanco " id="menu_entrenadores">
		<input id="btn_entrenadores" type="button" class="btn btn-primary more" onclick="nuevoEmpleado()" value="Nuevo Entrenador"/><br/><br/>
		<div class="row col s12 m12 l12">
                <div >
                    <h3 class="panel-title">Lista de Entrenadores</h3>
                </div>
                <div >
                    <table class="highlight centered">
                        <thead>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Apellido 1</th>
                        <th>Apellido 2</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Actividad</th>
                        <th>WhatsApp</th>
                        <th>Editar</th>
                        <th>Eliminar</th>            
                        </thead>
                        <tbody>
                            <?php
                           // require 'modelo/Database.php';
                            $pdo = Database::connect();
                            $sql = 'SELECT * FROM empleados';
                            $con = 1;
                            foreach ($pdo->query($sql) as $row) {
                                echo "<tr>";
                                echo '<td>' . $con . '</td>';
                                echo '<td>' . $row['nombre'] . '</td>';
                                echo '<td>' . $row['ape1'] . '</td>';
                                echo '<td>' . $row['ape2'] . '</td>';
                                echo '<td>' . $row['telefono'] . '</td>';
                                echo '<td>' . $row['email'] . '</td>';
                                echo '<td>' . $row['actividad'] . '</td>';

                                //variables xa whatssap
                            $tel=$row['telefono'];
                            $usu=$row['email'];  
                            $sqlDos = 'SELECT * FROM login where email= "'.$row['email'].'";';
                            $password="";
                            foreach ($pdo->query($sqlDos) as $contra) {
                                
                                $password=$contra['password'];
                            }
                            //desenriptamos la contra
                            $contra=base64_decode($password);
       
                                $mensaje =addslashes("YeFit,%20su%20usuario%20es:%20".$usu."%20y%20su%20Password:%20".$contra."");
                                echo '<td>
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=34'.$tel.'&text='.$mensaje.'">Recuperar Clave </a>
                                </td>';

                                echo '<td><button class="btn btn-primary" onclick="editaEmpleado(' . $row['id'] . ')"><i class="material-icons">mode_edit</i></button></td>';
                                echo '<td><button class="btn btn-primary" onclick="eliminaEmpleado(' . $row['id'] . ', this)"><i class="material-icons">delete</i></button></td>';
                                echo '</tr>';
                                $con++;
                            }
                            Database::disconnect();
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
	</div><!-- fin contenido_entrenadores-->


	<!-- ACTIVIDADES -->
		<div class="contenido_actividades texto_blanco row" id="menu_Actividades">
		<input id="btn_actividades" type="button" class="btn btn-primary more" onclick="nuevoActividad()" value="Nueva Actividad"/><br/><br/>
		<div class="row col s12 m12 l12">
                <div>
                    <h3 class="panel-title">Lista de Actividades</h3>
                </div>
                <div>
                    <table class="highlight centered">
                        <thead>
                        <th>#</th>
                        <th>Nombre</th>
                        <th>Dia</th>
                        <th>Hora</th>
                        <th>Aforo</th>
                        <th>Categoria</th>
                        <th>Editar</th>
                        <th>Eliminar</th>
                        <th>Informe Socios</th>
                        <th>Informe Entrenadores</th>            
                        </thead>
                        <tbody>
                            <?php
                            //require 'modelo/Database.php';
                            $pdo = Database::connect();
                            $sql = 'SELECT * FROM actividades';
                            $con = 1;
                            foreach ($pdo->query($sql) as $row) {
                                echo "<tr>";
                                echo '<td>' . $con . '</td>';
                                echo '<td>' . $row['nombre'] . '</td>';
                                echo '<td>' . $row['dia'] . '</td>';
                                echo '<td>' . $row['hora'] . '</td>';
                                echo '<td>' . $row['aforo'] . '</td>';
                                echo '<td>' . $row['categoria'] . '</td>';
                                echo '<td><button class="btn btn-primary" onclick="editaActividad(' . $row['id'] . ')"><i class="material-icons">mode_edit</i></button></td>';
                                echo '<td><button class="btn btn-primary" onclick="eliminaActividad(' . $row['id'] . ', this)"><i class="material-icons">delete</i></button></td>';

                               
                                echo '<td>

                                <form method="post" action="modelo/pdf.php" target="_blank">
                                    <input type="hidden" id="id_actividad" name="id_actividad" 
                                    value="'. $row[ "id"] .'">
                                    <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="Socios x actividad" >
                                    
                                
                                </form>
                                </td>';

                                echo '<td>

                                <form method="post" action="modelo/pdf_dos.php" target="_blank">
                                    <input type="hidden" id="nombre_actividad" name="nombre_actividad" 
                                    value="'. $row[ "nombre"] .'">
                                    <input type="submit" name="create_pdf" class="btn btn-danger pull-right" value="La imparten" >
                                    
                                
                                </form>
                                </td>';
                                $con++;
                            }
                            Database::disconnect();
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>




	</div><!-- fin contenido_Actividades-->





</div><!-- fin container-->	



		<!-- opciones x categoria en administracion-->
		
		<div id="menu_socios" class="row">
			<a  href="crud_socio_listar.php" class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">format_list_bulleted</i>Ver Socios</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class=" material-icons left icono-nav ">add</i>Nuevo Socio</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">edit</i>Editar Socio</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">clear</i>Borrar Socio</a>
		</div>

		<div id="menu_entrenadores" class="row">
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">format_list_bulleted</i>Ver Entrenadores</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class=" material-icons left icono-nav ">add</i>Nuevo Entrenador</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">edit</i>Editar Entrenador</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">clear</i>Borrar Entrenador</a>
		</div>

		<div id="menu_Actividades" class="row">
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">format_list_bulleted</i>Ver Actividades</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class=" material-icons left icono-nav ">add</i>Nuevo Actividad</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">edit</i>Editar Actividad</a>
			<a  class="waves-effect waves-light btn-large col s2 m2 l3"><i class="material-icons left icono-nav ">clear</i>Borrar Actividad</a>
		</div>
	



		
			
		
<!-- script para cada accion de editar/borrar/añadir-->	
<script>
	$(document).ready(function () {
    $('#btn_usuarios').click(function () {
        $.ajax({
            url: "administracion.php"
        }).done(function (html) {
            $('#contenido').html(html);
        }).fail(function () {
            alert('Error al cargar modulo');
        });
    });
});
    
    function editaUsuario(id){
        var parametros = {
                "operacion" : "update",
                "id_usuario" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/usuario.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}

function editaEmpleado(id){
        var parametros = {
                "operacion" : "update",
                "id_usuario" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/empleado.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}
function editaActividad(id){
        var parametros = {
                "operacion" : "update",
                "id_usuario" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/actividad.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}

 function eliminaUsuario(id){

    var confirmation = confirm("¿Está seguro de querer borrar este registro?");
    if(confirmation){

        var parametros = {
                "operacion" : "delete",
                "id_usuario" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/modifica_usuario.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });

        alert('Registro Borrado');
    }

}

function eliminaEmpleado(id){

    var confirmation = confirm("¿Está seguro de querer borrar este registro?");
    if(confirmation){

        var parametros = {
                "operacion" : "delete",
                "id_empleado" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/modifica_empleado.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });

        alert('Registro Borrado');
    }

}

function eliminaActividad(id){

    var confirmation = confirm("¿Está seguro de querer borrar este registro?");
    if(confirmation){

        var parametros = {
                "operacion" : "delete",
                "id_actividad" : id
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/modifica_actividad.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });

        alert('Registro Borrado');
    }

}

function nuevoUsuario(){
        var parametros = {
                "operacion" : "insert"
                
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/usuario.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}
function nuevoEmpleado(){
        var parametros = {
                "operacion" : "insert"
                
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/empleado.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}
function nuevoActividad(){
        var parametros = {
                "operacion" : "insert"
                
        };
        $.ajax({
                data:  parametros, //datos que se envian a traves de ajax
                url:   'modelo/actividad.php', //archivo que recibe la peticion
                type:  'post', //método de envio
                beforeSend: function () {
                        $("#resultado").html("Procesando, espere por favor...");
                },
                success:  function (response) { //una vez que el archivo recibe el request lo procesa y lo devuelve
                        $("#resultado").html(response);
                }
        });
}


</script>




    <!-- footer-->
     <?php  include $_SERVER['DOCUMENT_ROOT'].'/include/footer.php'; ?>
    </body>
</html>


